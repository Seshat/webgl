---
author: PF Villard
title: Génération automatique d'images à partir de webGL
date: \today
---

- Variable globale :
```
var strDownloadMime = "image/octet-stream";
```

- Paramétrisation du renderer :
```
renderer = new THREE.WebGLRenderer( { antialias: true, preserveDrawingBuffer: true } );
```
- Fonctions supplémentaires à rajouter :
```
function saveAsImage() {
    var imgData, imgNode;

    try {
        var strMime = "image/jpeg";
        imgData = renderer.domElement.toDataURL(strMime);

        saveFile(imgData.replace(strMime, strDownloadMime), "test.jpg");

    } catch (e) {
        console.log(e);
        return;
    }

}
var saveFile = function (strData, filename) {
    var link = document.createElement('a');
    if (typeof link.download === 'string') {
        document.body.appendChild(link); //Firefox requires the link to be in the body
        link.download = filename;
        link.href = strData;
        link.click();
        document.body.removeChild(link); //remove the link when done
    } else {
        location.replace(uri);
    }
}
```

- Utilisation : appeler les instructions suivantes à chaque fois qu'un rendu est nécessaire :
```
function animate() {
    window.requestAnimationFrame(animate);
    render();
    compte++;
    if (compte==100)
    {
        saveAsImage();
    }
}
```

Avec comme variable globale :
```
var compte=1;
```

<!-- pandoc -N --template=template.tex generationAutomatiqueImages.md --pdf-engine=xelatex --variable mainfont="Palatino" --variable sansfont="Helvetica" --variable monofont="Menlo" --variable fontsize=12pt    -o generationAutomatiqueImages.pdf -->