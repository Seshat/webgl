/*************************************************************/
/* 		SCRIPT POUR AFFICHER UN FICHIER 3D ET UNE SPHERE     */
/*************************************************************/


//Largeur et hauteur de la fenêtre
var largeur=600;
var hauteur=600;
// Scène, caméra et variable pour le rendu
var scene, camera, renderer;

// 1) Initialisation de la scène
init();
// 2) Animation éventuelle
animate();


// 1) Initialisation de la scène
//------------------------------
function init() {
	// Création d'une scène vide
	scene = new THREE.Scene();
	// Création d'une caméra avec projection perspective
	camera = new THREE.PerspectiveCamera( 75, largeur/hauteur, 0.1, 1000 );

	//Création d'une lumière dirrectionelle de direction (0,0,1) donc suivant Z
	var directionalLight = new THREE.DirectionalLight( 0xffeedd ); //Création
	directionalLight.position.set( 0, 0, 1 ); // Ajout de la direction (Essayer suivant X et Y)
	scene.add( directionalLight ); // Ajout à la scène

	camera.position.z = 5; // Changement de position de la caméra (Essayer de diminuer et augmenter)

	// MODELISATION 3D
	//     ----


	// a - Chargement d'un objet OBJ
	var loader = new THREE.OBJLoader();
	// Charger le fichier
	loader.load(
		'suzanne.obj',
		function ( object ) {
			object.traverse( function ( child ) {
        		if ( child instanceof THREE.Mesh ) {
        			/* Début du processus pour avoir du lissage */
        			 var geometry = new THREE.Geometry().fromBufferGeometry( child.geometry );
 					geometry.computeFaceNormals();
 					geometry.mergeVertices();
 					geometry.computeVertexNormals();
 					child.geometry = new THREE.BufferGeometry().fromGeometry( geometry );
 					/* Fin du processus pour avoir du lissage */
		            child.material = new THREE.MeshPhongMaterial( { color: 0x0000ff } );
        		}
        	});
			/* Fin du processus pour avoir du lissage */
			scene.add( object );
		}
	);

	// b - Ajout d'une cube
	var geometry = new THREE.BoxGeometry( 1, 1, 1 ); // Cube de coté = 1
	var material = new THREE.MeshPhongMaterial( { color: 0x00ff00 } ); // Couleur = vert
	var cube = new THREE.Mesh( geometry, material );
	cube.position.x=2; // Cube un peu décalé à droite
	cube.position.y=2; // Cube un peu décalé en haut
	cube.rotation.y=0.4; // Rotation du cube
	cube.rotation.z=0.2;
	scene.add( cube );


	// Création de la variable pour le rendu
	renderer = new THREE.WebGLRenderer();
	renderer.setSize( largeur, hauteur ); // Taille de l'image de rendu

	// Permet de tourner l'objet avec la souris
	controls = new THREE.OrbitControls( camera, renderer.domElement );

	camera.lookAt( scene.position );
	renderer.render( scene, camera );
}


// 2) Animation
//--------------
function animate() {
	// Lance l'animation
	requestAnimationFrame( animate );
	// Prend en compte l'interaction avec la souris
	controls.update();
	// Recalcule le rendu
	renderer.render( scene, camera )	
}




// Ajout du rendu à la page web
function addToDOM() {
	var container = document.getElementById("webGL");
	container.appendChild( renderer.domElement );
}


