var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera( 50, window.innerWidth/window.innerHeight, 0.1, 1000 );
camera.position.z = 40;

var renderer = new THREE.WebGLRenderer();
renderer.setSize( 800, 800 );

var mesh= new Array(5);
var speed= new Array(10);
/* Controls */

controls = new THREE.OrbitControls(camera, renderer.domElement);
controls.enableDamping = true;
controls.dampingFactor = 0.25;
controls.enableZoom = false;

var stats = new Stats();
stats.showPanel( 0 ); // 0: fps, 1: ms, 2: mb, 3+: custom
document.body.appendChild( stats.dom );

light = new THREE.DirectionalLight(0xffffff, 0.5);
light.position.set(1,1,0)
scene.add(light);

light2 = new THREE.DirectionalLight(0xffffff, 1.0);
light2.position.set(-1,1,0)
//scene.add(light2);

light2 = new THREE.DirectionalLight(0xffffff, 0.5);
light2.position.set(0,0,1)
scene.add(light2);

// Create cube render target
const cubeRenderTarget = new THREE.WebGLCubeRenderTarget( 1024, { format: THREE.RGBFormat, generateMipmaps: true, minFilter: THREE.LinearMipmapLinearFilter } );


renderer.setClearColor( 0xffffff, 1.0 );

for (var cpt=0;cpt<20;cpt++)
{
    var material = new THREE.MeshPhongMaterial();
    material.color.r=1;
    material.color.g=1;    
    material.color.b=1;
    var radius=2*Math.random()+1;
    var sphere = new THREE.SphereGeometry(  radius, 32, 16);
    mesh[cpt] = new THREE.Mesh( sphere, material );
    mesh[cpt].position.x=10-(20*Math.random());
    mesh[cpt].position.z=10-(20*Math.random());
    mesh[cpt].position.y=10-(20*Math.random());
    speed[cpt] = [];
    speed[cpt][0]=2*Math.random()-1;
    speed[cpt][1]=2*Math.random()-1;
    speed[cpt][2]=2*Math.random()-1;
    scene.add(mesh[cpt]);
}



    // Miroir
    // Avec un plan
    var cubeGeom = new THREE.BoxGeometry(40, 40, 0.01);
    // Avec une sphere
    //var cubeGeom = new THREE.SphereGeometry(20,32, 32);

    mirrorCam = new THREE.CubeCamera( 25, 50, cubeRenderTarget   );
    mirrorCam.position.set(0, -0, -50); 
    scene.add(mirrorCam);


    var mirrorCubeMaterial = new THREE.MeshLambertMaterial( { color: 0xffffff, envMap: cubeRenderTarget.texture } );
    mirrorCube = new THREE.Mesh( cubeGeom, mirrorCubeMaterial );
    mirrorCube.position.set(0, -0, -15);
    scene.add(mirrorCube);



render();



function animate() {

    requestAnimationFrame(animate);

    controls.update();

for (var cpt=0;cpt<20;cpt++)
{
    mesh[cpt].position.x=mesh[cpt].position.x+0.1*speed[cpt][0];
    mesh[cpt].position.y=mesh[cpt].position.y+0.1*speed[cpt][1];
    mesh[cpt].position.z=mesh[cpt].position.z+0.1*speed[cpt][2];
    if (mesh[cpt].position.x<-10)
    {
        speed[cpt][0]=-speed[cpt][0];
    }
    if (mesh[cpt].position.y<-10)
    {
        speed[cpt][1]=-speed[cpt][1];
    }
    if (mesh[cpt].position.z<-10)
    {
        speed[cpt][2]=-speed[cpt][2];
    }
    if (mesh[cpt].position.x>10)
    {
        speed[cpt][0]=-speed[cpt][0];
    }
    if (mesh[cpt].position.y>10)
    {
        speed[cpt][1]=-speed[cpt][1];
    }            
    if (mesh[cpt].position.z>10)
    {
        speed[cpt][2]=-speed[cpt][2];
    }
    stats.update();
}


    render();

}

function render() {

   mirrorCam.update( renderer, scene );
    renderer.render(scene, camera);

}

function ajout(){

}

function addToDOM() {
    var container = document.getElementById("webGL");
    container.appendChild( renderer.domElement );
}

animate();