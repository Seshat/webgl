var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera( 50, window.innerWidth/window.innerHeight, 0.1, 1000 );
camera.position.z = 40;

var renderer = new THREE.WebGLRenderer();
renderer.setSize( 800, 800 );

var mesh= new Array(5);
var speed= new Array(10);
/* Controls */

controls = new THREE.OrbitControls(camera, renderer.domElement);
controls.enableDamping = true;
controls.dampingFactor = 0.25;
controls.enableZoom = false;

var stats = new Stats();
stats.showPanel( 0 ); // 0: fps, 1: ms, 2: mb, 3+: custom
document.body.appendChild( stats.dom );

//scene.add( new THREE.AmbientLight( 0x222222 ) );

light = new THREE.DirectionalLight(0xffffff, 1.0);
light.position.set(1,1,0)
scene.add(light);

light2 = new THREE.DirectionalLight(0xffffff, 1.0);
light2.position.set(-1,1,0)
scene.add(light2);

// var width=5;
// var height=4;
// var material = new THREE.MeshBasicMaterial( { color: 0xff0000 } );
// var cube = new THREE.CubeGeometry( width, height);
// var cubeMesh = new THREE.Mesh( cube, material );
// scene.add(cubeMesh);

renderer.setClearColor( 0x000000, 1.0 );

for (var cpt=0;cpt<20;cpt++)
{
    var material = new THREE.MeshPhongMaterial();
    material.color.r=1;
    material.color.g=1;    
    material.color.b=1;
    var radius=3*Math.random()+1;
    var sphere = new THREE.SphereGeometry(  radius, 32, 16);
    mesh[cpt] = new THREE.Mesh( sphere, material );
    mesh[cpt].position.x=10-(20*Math.random());
    mesh[cpt].position.z=10-(20*Math.random());
    mesh[cpt].position.y=10-(20*Math.random());
    speed[cpt] = [];
    speed[cpt][0]=2*Math.random()-1;
    speed[cpt][1]=2*Math.random()-1;
    speed[cpt][2]=2*Math.random()-1;
    mesh[cpt].castShadow = true;
    mesh[cpt].receiveShadow = true;
    scene.add(mesh[cpt]);
}

renderer.shadowMap.enabled = true;
light.castShadow = true;
light2.castShadow = true;

function animate() {

    requestAnimationFrame(animate);

    controls.update();

for (var cpt=0;cpt<20;cpt++)
{
    mesh[cpt].position.x=mesh[cpt].position.x+0.1*speed[cpt][0];
    mesh[cpt].position.y=mesh[cpt].position.y+0.1*speed[cpt][1];
    mesh[cpt].position.z=mesh[cpt].position.z+0.1*speed[cpt][2];
    if (mesh[cpt].position.x<-10)
    {
        speed[cpt][0]=-speed[cpt][0];
    }
    if (mesh[cpt].position.y<-10)
    {
        speed[cpt][1]=-speed[cpt][1];
    }
    if (mesh[cpt].position.z<-10)
    {
        speed[cpt][2]=-speed[cpt][2];
    }
    if (mesh[cpt].position.x>10)
    {
        speed[cpt][0]=-speed[cpt][0];
    }
    if (mesh[cpt].position.y>10)
    {
        speed[cpt][1]=-speed[cpt][1];
    }            
    if (mesh[cpt].position.z>10)
    {
        speed[cpt][2]=-speed[cpt][2];
    }
    stats.update();
}


    render();

}

function render() {

    renderer.render(scene, camera);

}

function ajout(){

}

function addToDOM() {
    var container = document.getElementById("webGL");
    container.appendChild( renderer.domElement );
}

animate();