var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera( 50, window.innerWidth/window.innerHeight, 0.1, 1000 );
camera.position.z = 40;

var renderer = new THREE.WebGLRenderer();
renderer.setSize( 800, 800 );

var mesh= new Array(5);
var speed= new Array(10);
/* Controls */

controls = new THREE.OrbitControls(camera, renderer.domElement);
controls.enableDamping = true;
controls.dampingFactor = 0.25;
controls.enableZoom = false;

var stats = new Stats();
stats.showPanel( 0 ); // 0: fps, 1: ms, 2: mb, 3+: custom
document.body.appendChild( stats.dom );

//scene.add( new THREE.AmbientLight( 0x222222 ) );

// light = new THREE.DirectionalLight(0xffffff, 1.0);
// light.position.set(-1,-1,0)
// scene.add(light);

// var width=5;
// var height=4;
// var material = new THREE.MeshBasicMaterial( { color: 0xff0000 } );
// var cube = new THREE.CubeGeometry( width, height);
// var cubeMesh = new THREE.Mesh( cube, material );
// scene.add(cubeMesh);

renderer.setClearColor( 0x000000, 1.0 );

for (var cpt=0;cpt<10;cpt++)
{
    var material = new THREE.MeshPhongMaterial();
    material.color.r=1;
    material.color.g=1;    
    material.color.b=1;
    var radius=2;
    var sphere = new THREE.SphereGeometry(  radius, 32, 16);
    mesh[cpt] = new THREE.Mesh( sphere, material );
    mesh[cpt].position.x=10-(20*Math.random());
    mesh[cpt].position.z=10-(20*Math.random());
    mesh[cpt].position.y=10-(20*Math.random());
    speed[cpt] = [];
    speed[cpt][0]=2*Math.random()-1;
    speed[cpt][1]=2*Math.random()-1;
    speed[cpt][2]=2*Math.random()-1;
    scene.add(mesh[cpt]);
}


for (var cpt=0;cpt<100;cpt++)
{
    var r=Math.random();
    var g=Math.random();    
    var b=Math.random();
    var x=10-(20*Math.random());
    var z=10-(20*Math.random());
    var y=10-(20*Math.random());

    var ptLight = new THREE.PointLight(2);
    ptLight.distance=5;
    ptLight.color.r=r;
    ptLight.color.g=g;
    ptLight.color.b=b;
    ptLight.position.set(x,y,z)
    scene.add(ptLight);


    var material = new THREE.MeshPhongMaterial();
    material.emissive.r=r;
    material.emissive.g=g;    
    material.emissive.b=b;
    var radius=0.5;
    var sphere = new THREE.SphereGeometry(  radius, 32, 16);
    ptLightSphere = new THREE.Mesh( sphere, material );
    ptLightSphere.position.x=x;
    ptLightSphere.position.z=y;
    ptLightSphere.position.y=z;
    scene.add(ptLightSphere);
}

renderer.forwardRendering = true;
//renderer.enableLightPrePass( true );

function animate() {

    requestAnimationFrame(animate);

    controls.update();

for (var cpt=0;cpt<10;cpt++)
{
    mesh[cpt].position.x=mesh[cpt].position.x+0.1*speed[cpt][0];
    mesh[cpt].position.y=mesh[cpt].position.y+0.1*speed[cpt][1];
    mesh[cpt].position.z=mesh[cpt].position.z+0.1*speed[cpt][2];
    if (mesh[cpt].position.x<-10)
    {
        speed[cpt][0]=-speed[cpt][0];
    }
    if (mesh[cpt].position.y<-10)
    {
        speed[cpt][1]=-speed[cpt][1];
    }
    if (mesh[cpt].position.z<-10)
    {
        speed[cpt][2]=-speed[cpt][2];
    }
    if (mesh[cpt].position.x>10)
    {
        speed[cpt][0]=-speed[cpt][0];
    }
    if (mesh[cpt].position.y>10)
    {
        speed[cpt][1]=-speed[cpt][1];
    }            
    if (mesh[cpt].position.z>10)
    {
        speed[cpt][2]=-speed[cpt][2];
    }
    stats.update();
}


    render();

}

function render() {

    renderer.render(scene, camera);

}

function ajout(){

}

function addToDOM() {
    var container = document.getElementById("webGL");
    container.appendChild( renderer.domElement );
}

animate();