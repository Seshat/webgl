"use strict"; // good practice - see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
////////////////////////////////////////////////////////////////////////////////
// Vertex Order Exercise
// Your task is to determine the problem and fix the vertex drawing order.
// Check the function someObject()
// and correct the code that starts at line 17.
////////////////////////////////////////////////////////////////////////////////
/*global THREE, Coordinates, $, document*/
import * as THREE from 'three';

import { OBJLoader } from 'three/addons/loaders/OBJLoader.js';
import { OrbitControls } from 'three/addons/controls/OrbitControls.js';
import {dat} from './lib/dat.gui.min.js';

var camera, renderer;
var windowScale;
window.scene = new THREE.Scene();
import {Coordinates} from './lib/Coordinates.js';

function someObject(material) {
	var geometry = new THREE.BufferGeometry();

	// Student: some data below must be fixed
	// for both triangles to appear !
	const vertices = new Float32Array( [
		3, 3, 0,
		7, 3, 0,
		7, 7, 0,

		7, 7, 0,
		3, 7, 0, 
		3, 3, 0
	] );



	geometry.setAttribute( 'position', new THREE.BufferAttribute( vertices, 3 ) );


	var mesh = new THREE.Mesh( geometry, material );

	scene.add( mesh );
}

function init() {
	// Setting up some parameters
	var canvasWidth = 846;
	var canvasHeight = 494;
	// For grading the window is fixed in size; here's general code:
	//var canvasWidth = window.innerWidth;
	//var canvasHeight = window.innerHeight;
	var canvasRatio = canvasWidth / canvasHeight;
	// scene
	scene = new THREE.Scene();

	// Camera: Y up, X right, Z up
	windowScale = 10;
	var windowWidth = windowScale * canvasRatio;
	var windowHeight = windowScale;

	camera = new THREE.OrthographicCamera( windowWidth / - 2, windowWidth / 2,
		windowHeight / 2, windowHeight / - 2, 0, 40 );

	var focus = new THREE.Vector3( 5,4,0 );
	camera.position.x = focus.x;
	camera.position.y = focus.y;
	camera.position.z = 10;
	camera.lookAt( focus );

	renderer = new THREE.WebGLRenderer({ antialias: true, preserveDrawingBuffer: true});
	renderer.gammaInput = true;
	renderer.gammaOutput = true;
	renderer.setSize( canvasWidth, canvasHeight );
	renderer.setClearColor(new THREE.Color(0xFFFFFF),1.0);

}


function showGrids() {
	// Background grid and axes. Grid step size is 1, axes cross at 0, 0
	Coordinates.drawGrid({size:100,scale:1,orientation:"z"});
	Coordinates.drawAxes({axisLength:11,axisOrientation:"x",axisRadius:0.04});
	Coordinates.drawAxes({axisLength:11,axisOrientation:"y",axisRadius:0.04});
}

function render() {
	renderer.render( scene, camera );
}


// Main body of the script
try {
	init();
	showGrids();
	var material = new THREE.MeshBasicMaterial( { color: 0xF6831E, side: THREE.FrontSide } );
	someObject(material);
	addToDOM();
	render();
} catch(e) {
	var errorReport = "Error:</br>"+e;
	var node = document.createTextNode(errorReport);
	var container = document.getElementById("webGL");
	container.appendChild(node);
}


// Ajout du rendu à la page web
function addToDOM() {
	var container = document.getElementById("webGL");
	container.appendChild( renderer.domElement );
}

