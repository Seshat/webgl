"use strict"; // good practice - see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
////////////////////////////////////////////////////////////////////////////////
// 
////////////////////////////////////////////////////////////////////////////////
/*global THREE, Coordinates, document*/
import * as THREE from 'three';

import { OBJLoader } from 'three/addons/loaders/OBJLoader.js';
import { OrbitControls } from 'three/addons/controls/OrbitControls.js';
import {dat} from './lib/dat.gui.min.js';

var camera, renderer;
var windowScale;
window.scene = new THREE.Scene();

import {Coordinates} from './lib/Coordinates.js';

function medievalWall(l,h) {


	var triangleMaterial = new THREE.MeshBasicMaterial( { color: 0x2685AA, side: THREE.DoubleSide } );
	// This code demonstrates how to draw a triangle
	var triangle = new THREE.BufferGeometry();
	
	const vertices = new Float32Array( [
		0.0, 0.0,  0.0,
		l, 0.0, 0.0,
		l, h,  0.0,
		0.0, 0.0,  0.0,
		l, h, 0.0,
		0.0, h,  0.0,
	] );
	triangle.setAttribute( 'position', new THREE.BufferAttribute( vertices, 3 ) );
	var triangleMesh = new THREE.Mesh( triangle, triangleMaterial );
	window.scene.add(triangleMesh);

	var  carres=new Array(l/2);
	var  geom=new Array(l/2);
	for(var cpt=0;cpt<l/2.;cpt++)
	{
		carres[cpt] = new THREE.BufferGeometry();
		var carre = new Float32Array( [
			cpt*2, h,  0.0,
			cpt*2+1, h, 0.0,
			cpt*2+1, h+1,  0.0,
			cpt*2, h,  0.0,
			cpt*2+1, h+1, 0.0,
			cpt*2, h+1,  0.0,
		] );
		carres[cpt].setAttribute( 'position', new THREE.BufferAttribute( carre, 3 ) );
		window.scene.add(new THREE.Mesh( carres[cpt], triangleMaterial ));
	}
}


function drawSquare(x1, y1, x2, y2) {

	var square = new THREE.BufferGeometry();
	// Your code goes here

	// don't forget to return the geometry!	The following line is required!
	return square;
}

function init() {
	// Set up some parameters
	var canvasWidth = 846;
	var canvasHeight = 494;
	// For grading the window is fixed in size; here's general code:
	//var canvasWidth = window.innerWidth;
	//var canvasHeight = window.innerHeight;
	var canvasRatio = canvasWidth / canvasHeight;

	// Camera: Y up, X right, Z up
	windowScale = 12;
	var windowWidth = windowScale * canvasRatio;
	var windowHeight = windowScale;

	camera = new THREE.OrthographicCamera(windowWidth/-2, windowWidth/2, windowHeight/2, windowHeight/-2, 0, 40);

	var focus = new THREE.Vector3( 7,5,0 );
	camera.position.x = focus.x;
	camera.position.y = focus.y;
	camera.position.z = 20;
	camera.lookAt(focus);

	renderer = new THREE.WebGLRenderer({ antialias: true, preserveDrawingBuffer: true});
	renderer.gammaInput = true;
	renderer.gammaOutput = true;
	renderer.setSize( canvasWidth, canvasHeight );
	renderer.setClearColor(new THREE.Color(0xFFFFFF),1.0);
}

function render() {
	renderer.render(  window.scene, camera );
}

function showGrids() {
	// Background grid and axes. Grid step size is 1, axes cross at 0, 0
	Coordinates.drawGrid({size:100,scale:1,orientation:"z"});
	Coordinates.drawAxes({axisLength:11,axisOrientation:"x",axisRadius:0.04});
	Coordinates.drawAxes({axisLength:11,axisOrientation:"y",axisRadius:0.04});
}

try {
	init();
	showGrids();
	// creating and adding the triangle to the  window.scene
	medievalWall(16,5);
	
	render();
} catch(e) {
	var errorReport = "Error:</br>"+e;
	var node = document.createTextNode(errorReport);
	var container = document.getElementById("webGL");
	container.appendChild(node);
}


var container = document.createElement( 'div' );
document.body.appendChild( container );
container.appendChild( renderer.domElement );


