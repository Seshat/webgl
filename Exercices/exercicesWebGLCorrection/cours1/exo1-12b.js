"use strict"; // good practice - see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
////////////////////////////////////////////////////////////////////////////////
// 
////////////////////////////////////////////////////////////////////////////////
/*global THREE, Coordinates, document*/
import * as THREE from 'three';

import { OBJLoader } from 'three/addons/loaders/OBJLoader.js';
import { OrbitControls } from 'three/addons/controls/OrbitControls.js';
import {dat} from './lib/dat.gui.min.js';

var camera, renderer, cameraControls;
var windowScale;
window.scene = new THREE.Scene();
var clock = new THREE.Clock();

import {Coordinates} from './lib/Coordinates.js';

function exampleTriangle() {
	// This code demonstrates how to draw a triangle
	var triangle = new THREE.BufferGeometry();

	const vertices = new Float32Array( [
		// Z=0
		1.0, 1.0,  0.0,	
		3.0, 3.0,  0.0,
		3.0, 1.0, 0.0,
		1.0, 1.0,  0.0,
		1.0, 3.0,  0.0,
		3.0, 3.0, 0.0,
		// Z=2	
		1.0, 1.0,  2.0,
		3.0, 1.0, 2.0,
		3.0, 3.0,  2.0,
		1.0, 1.0,  2.0,
		3.0, 3.0, 2.0,
		1.0, 3.0,  2.0,
		// X=1
		1.0, 1.0,  0.0,		
		1.0, 1.0, 2.0,
		1.0, 3.0,  2.0,
		1.0, 1.0,  0.0,
		1.0, 3.0, 2.0,
		1.0, 3.0,  0.0,
		// X=3	
		3.0, 1.0,  0.0,	
		3.0, 3.0,  2.0,
		3.0, 1.0, 2.0,
		3.0, 1.0,  0.0,
		3.0, 3.0,  0.0,
		3.0, 3.0, 2.0,		
		// Y=1
		1.0, 1.0,  0.0,		
		3.0, 1.0,  2.0,
		1.0, 1.0, 2.0,
		1.0, 1.0,  0.0,	
		3.0, 1.0,  0.0,
		3.0, 1.0, 2.0,
		// Y=3	
		1.0, 3.0,  0.0,		
		1.0, 3.0, 2.0,
		2.0, 4.0,  2.0,
		1.0, 3.0,  0.0,
		2.0, 4.0, 2.0,
		2.0, 4.0,  0.0,

		3.0, 3.0,  0.0,		
		3.0, 3.0, 2.0,
		2.0, 4.0,  2.0,
		3.0, 3.0,  0.0,
		2.0, 4.0, 2.0,
		2.0, 4.0,  0.0,
	] );

	triangle.setAttribute( 'position', new THREE.BufferAttribute( vertices, 3 ) );

	return triangle;
}



function init() {
	// Set up some parameters
	var canvasWidth = 846;
	var canvasHeight = 494;
	// For grading the window is fixed in size; here's general code:
	//var canvasWidth = window.innerWidth;
	//var canvasHeight = window.innerHeight;
	var canvasRatio = canvasWidth / canvasHeight;

	// Camera: Y up, X right, Z up
	windowScale = 12;
	var windowWidth = windowScale * canvasRatio;
	var windowHeight = windowScale;

	camera = new THREE.OrthographicCamera(windowWidth/-2, windowWidth/2, windowHeight/2, windowHeight/-2, 0, 40);

	var focus = new THREE.Vector3( 5,5,0 );
	camera.position.x = focus.x;
	camera.position.y = focus.y;
	camera.position.z = 20;
	camera.lookAt(focus);

	renderer = new THREE.WebGLRenderer({ antialias: true, preserveDrawingBuffer: true});
	renderer.gammaInput = true;
	renderer.gammaOutput = true;
	renderer.setSize( canvasWidth, canvasHeight );
	renderer.setClearColor(new THREE.Color(0xFFFFFF),1.0);
	 // CONTROLS
	 cameraControls = new OrbitControls(camera, renderer.domElement);
	 cameraControls.target.set(0, 0, 0);
}

function render() {
	var delta = clock.getDelta();
    cameraControls.update(delta);
    renderer.render(window.scene, camera);
}

function animate() {
	requestAnimationFrame(animate);
	render();
}

function showGrids() {
	// Background grid and axes. Grid step size is 1, axes cross at 0, 0
	Coordinates.drawGrid({size:100,scale:1,orientation:"z"});
	Coordinates.drawAxes({axisLength:11,axisOrientation:"x",axisRadius:0.04});
	Coordinates.drawAxes({axisLength:11,axisOrientation:"y",axisRadius:0.04});
}

// Ajout du rendu à la page web
function addToDOM() {
	var container = document.getElementById("webGL");
	container.appendChild( renderer.domElement );
}

try {
	init();
	showGrids();
	// creating and adding the triangle to the  window.scene
	var triangleMaterial = new THREE.MeshBasicMaterial( { color: 0x2685AA, side: THREE.DoubleSide } );
	var triangleGeometry = exampleTriangle();
	var triangleMesh = new THREE.Mesh( triangleGeometry, triangleMaterial );
	 window.scene.add(triangleMesh);

	 addToDOM()
	 animate();
} catch(e) {
	var errorReport = "Error:</br>"+e;
	var node = document.createTextNode(errorReport);
	var container = document.getElementById("webGL");
	container.appendChild(node);
}


