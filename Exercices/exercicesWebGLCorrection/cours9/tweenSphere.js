var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera( 50, window.innerWidth/window.innerHeight, 0.1, 1000 );
camera.position.z = 40;
var container;
var renderer = new THREE.WebGLRenderer({antialiasing:true});
renderer.setSize( 800, 800 );

var objects=[];

var mesh= new Array(5);
var speed= new Array(10);
/* Controls */

// var sphere = new THREE.Mesh( new THREE.SphereGeometry(2,18,18), 
//                                       new THREE.MeshPhongMaterial());

controls = new THREE.OrbitControls(camera, renderer.domElement);
controls.enableDamping = true;
controls.dampingFactor = 0.25;
controls.enableZoom = false;

var stats = new Stats();
stats.showPanel( 0 ); // 0: fps, 1: ms, 2: mb, 3+: custom
document.body.appendChild( stats.dom );

//scene.add( new THREE.AmbientLight( 0x555555 ) );

light = new THREE.DirectionalLight(0xffffff, 0.5);
light.position.set(1,1,0)
scene.add(light);

light2 = new THREE.DirectionalLight(0xffffff, 1.0);
light2.position.set(-1,1,0)
//scene.add(light2);

light2 = new THREE.DirectionalLight(0xffffff, 0.5);
light2.position.set(0,0,1)
scene.add(light2);

// scene.add(sphere);
    // initial setup of the tweens
    setupTween();




renderer.setClearColor( 0xffffff, 1.0 );

for (var cpt=0;cpt<20;cpt++)
{
    var material = new THREE.MeshPhongMaterial();
    material.color.r=Math.random();
    material.color.g=Math.random();    
    material.color.b=Math.random();
    var radius=2*Math.random()+1;
    var sphere2 = new THREE.SphereGeometry(  radius, 32, 32);
    mesh[cpt] = new THREE.Mesh( sphere2, material );
    mesh[cpt].position.x=10-(20*Math.random());
    mesh[cpt].position.z=10-(20*Math.random());
    mesh[cpt].position.y=10-(20*Math.random());
    speed[cpt] = [];
    speed[cpt][0]=2*Math.random()-1;
    speed[cpt][1]=2*Math.random()-1;
    speed[cpt][2]=2*Math.random()-1;
    scene.add(mesh[cpt]);
}
render();




// ## =========================

// ## Tween.js Setup (Start here)

// ## =========================

function setupTween()
{
    // 
    var update  = function(){
        var scale = (6+current.x)/6;
        for (var cpt=0;cpt<20;cpt++)
        {
            mesh[cpt].position.x=mesh[cpt].position.x+0.05*speed[cpt][0]*current.x;
            mesh[cpt].position.y=mesh[cpt].position.y+0.1*speed[cpt][1]*current.x;
            mesh[cpt].scale.x=scale;
            mesh[cpt].scale.y=scale;
            mesh[cpt].scale.z=scale;
        }
    }
    var current = { x: -2 };

    // remove previous tweens if needed
    //TWEEN.removeAll();
    // maybe replace that by window... or something
var userOpts	= {
	range		: 800,
	duration	: 2500,
	delay		: 200,
	easing		: 'Elastic.EaseInOut'
};


    var easing	= TWEEN.Easing[userOpts.easing.split('.')[0]][userOpts.easing.split('.')[1]];
    // build the tween to go ahead
    var tweenHead   = new TWEEN.Tween(current)
        .to({x: +2}, 2500)
        .delay(200)
        .easing(easing)
        .onUpdate(update);
    // build the tween to go backward
    var tweenBack   = new TWEEN.Tween(current)
        .to({x: -2}, 2500)
        .delay(200)
        .easing(easing)
        .onUpdate(update);

    // after tweenHead do tweenBack
    tweenHead.chain(tweenBack);
    // after tweenBack do tweenHead, so it is cycling
    tweenBack.chain(tweenHead);

    // start the first
    tweenHead.start();
}

// ## =========================

// ## Tween.js Setup (End here)

// ## =========================



function animate() {

    requestAnimationFrame(animate);
    TWEEN.update();
    stats.update();
    render();

}

function render() {
    renderer.render(scene, camera);

}

function ajout(){

}

function addToDOM() {
    container = document.getElementById("webGL");
    container.appendChild( renderer.domElement );
    $('#webGL').append(errorReport+e);
}

animate();