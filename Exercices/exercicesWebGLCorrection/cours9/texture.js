var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera( 50, window.innerWidth/window.innerHeight, 0.1, 1000 );
camera.position.z = 40;
var container;
var renderer = new THREE.WebGLRenderer();
renderer.setSize( 800, 800 );

var objects=[];

var mesh= new Array(5);
var speed= new Array(10);
/* Controls */

// var sphere = new THREE.Mesh( new THREE.SphereGeometry(2,18,18), 
//                                       new THREE.MeshPhongMaterial());

controls = new THREE.OrbitControls(camera, renderer.domElement);
controls.enableDamping = true;
controls.dampingFactor = 0.25;
controls.enableZoom = false;

var stats = new Stats();
stats.showPanel( 0 ); // 0: fps, 1: ms, 2: mb, 3+: custom
document.body.appendChild( stats.dom );

scene.add( new THREE.AmbientLight( 0x555555 ) );

light = new THREE.DirectionalLight(0xffffff, 0.5);
light.position.set(1,1,0)
scene.add(light);

light2 = new THREE.DirectionalLight(0xffffff, 1.0);
light2.position.set(-1,1,0)
//scene.add(light2);

light2 = new THREE.DirectionalLight(0xffffff, 0.5);
light2.position.set(0,0,1)
scene.add(light2);

var black =  new THREE.MeshPhongMaterial({color: 0x000000});

var texture = new THREE.TextureLoader().load('textures/wood.jpg');
texture.wrapS = texture.wrapT = THREE.MirroredRepeatWrapping;
texture.repeat.set( 2, 2 );

var materials =  new THREE.MeshPhongMaterial({map: texture});
materials.side = THREE.DoubleSide;                 

var geometry = new THREE.PlaneGeometry(20, 20);
var floor= new THREE.Mesh(geometry,materials);
scene.add(floor);

var floor2= new THREE.Mesh(geometry,materials);
floor2.rotation.x=3.14159/2.;
floor2.position.set(0,-10,+10);
scene.add(floor2);

var floor3= new THREE.Mesh(geometry,materials);
floor3.rotation.x=3.14159/2.;
floor3.position.set(0,+10,+10);
scene.add(floor3);


var floor4= new THREE.Mesh(geometry,materials);
floor4.rotation.y=3.14159/2.;
floor4.position.set(-10,0,+10);
scene.add(floor4);


var floor5= new THREE.Mesh(geometry,materials);
floor5.rotation.y=3.14159/2.;
floor5.position.set(10,0,10);
scene.add(floor5);



///////////
    // VIDEO //
    ///////////

var videoFiles=['textures/fire.mp4',
                'textures/jukebox.mp4',
                'textures/saturdayNightFever.mp4',
                'textures/window.mp4',
                'textures/window2.mp4'];
var videos=[];
var videoTextures=[];
var videoImageContext=[];
var movieMaterial=[];
var width=[256,256,256,256,256];
var height=[256,256,256,256,256];
for (var cpt=0;cpt<videoFiles.length;cpt++)
{
    videos[cpt] = document.createElement( 'video' );
    videos[cpt] .src = videoFiles[cpt];
    videos[cpt] .load(); // must call after setting/changing source
    videos[cpt] .play();
    videos[cpt] .loop=true;
    
    videoImage = document.createElement( 'canvas' );
    videoImage.width = width[cpt];
    videoImage.height = height[cpt];

    videoImageContext[cpt] = videoImage.getContext( '2d' );
    videoImageContext[cpt].fillRect( 0, 0, videoImage.width, videoImage.height );
    videoTextures[cpt] = new THREE.Texture( videoImage );

    movieMaterial[cpt] = new THREE.MeshBasicMaterial( { map: videoTextures[cpt], side:THREE.DoubleSide,transparent:true } );
}

    
    var box1=new THREE.Mesh(new THREE.BoxGeometry(7,9,2),materials);
    box1.position.set(-4,-5,1.9);
    scene.add(box1);
    var movieGeometry = new THREE.PlaneGeometry( 6, 6, 4, 4 );
    var movieScreen = new THREE.Mesh( movieGeometry, movieMaterial[0] );
    movieScreen.position.set(-4,-4,3);
    scene.add(movieScreen);

    box1=new THREE.Mesh(new THREE.BoxGeometry(7,10,2),black);
    box1.position.set(5,-5,0.9);
    scene.add(box1);
    var movieGeometry = new THREE.PlaneGeometry( 7, 10, 4, 4 );
    var movieScreen = new THREE.Mesh( movieGeometry, movieMaterial[1] );
    movieScreen.position.set(5,-5,2);
    scene.add(movieScreen);    

    box1=new THREE.Mesh(new THREE.BoxGeometry(4,3,2),black);
    box1.position.set(-4,1,2);
    scene.add(box1);
    var movieGeometry = new THREE.PlaneGeometry( 4, 3, 4 );
    var movieScreen = new THREE.Mesh( movieGeometry, movieMaterial[2] );
    movieScreen.position.set(-4,1,3.1);
    scene.add(movieScreen); 

    var geometry2 = new THREE.BoxGeometry(0.2, 8,8);
    var cube= new THREE.Mesh(geometry2,movieMaterial[3]);
    cube.position.set(-10,0,5);
    scene.add(cube);

    var geometry2 = new THREE.BoxGeometry(8, 8,0.2);
    var cube= new THREE.Mesh(geometry2,movieMaterial[4]);
    cube.position.set(5,5,0);
    scene.add(cube);


renderer.setClearColor( 0x000000, 1.0 );
//renderer.setClearColor( 0xffffff, 1.0 );

render();




function animate() {

    requestAnimationFrame(animate);
    stats.update();

    render();

}

function render() {
    for (var cpt=0;cpt<videos.length;cpt++)
    {
        if ( videos[cpt] .readyState === videos[cpt] .HAVE_ENOUGH_DATA ) 
            {
                videoImageContext[cpt].drawImage( videos[cpt] , 0, 0 );
                if ( videoTextures[cpt] ) 
                    videoTextures[cpt].needsUpdate = true;
            }
    }
    renderer.render(scene, camera);

}

function ajout(){

}

function addToDOM() {
    container = document.getElementById("webGL");
    container.appendChild( renderer.domElement );
}

animate();