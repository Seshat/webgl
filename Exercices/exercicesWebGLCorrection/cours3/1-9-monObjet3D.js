/*************************************************************/
/* 		SCRIPT POUR AFFICHER UN FICHIER 3D ET UNE SPHERE     */
/*************************************************************/




//Largeur et hauteur de la fenêtre
var largeur=600;
var hauteur=600;
// Scène, caméra et variable pour le rendu
var scene, camera, renderer;

// 1) Initialisation de la scène
init();
// 2) Animation éventuelle
animate();


// 1) Initialisation de la scène
//------------------------------
function init() {
	// Création d'une scène vide
	scene = new THREE.Scene();
	// Création d'une caméra avec projection perspective
	camera = new THREE.PerspectiveCamera( 75, largeur/hauteur, 0.1, 1000 );

	//Création d'une lumière dirrectionelle de direction (0,0,1) donc suivant Z
	var directionalLight = new THREE.DirectionalLight( 0xffeedd ); //Création
	directionalLight.position.set( 0, 1, 1 ); // Ajout de la direction (Essayer suivant X et Y)
	scene.add( directionalLight ); // Ajout à la scène

	camera.position.y = 3; // Changement de position de la caméra (Essayer de diminuer et augmenter)
	camera.position.z = 10;

var loader = new THREE.ColladaLoader();	
var X=Y=0;
sphereRadius=4;

// Create a basic rectangle geometry
var planeGeometry = new THREE.PlaneGeometry(300, 100);
var planeMaterial = new THREE.MeshLambertMaterial({color: 0x00ff00, side: THREE.DoubleSide});
var dispPlane = new THREE.Mesh(planeGeometry, planeMaterial);
dispPlane.rotation.x=-Math.PI/2;
scene.add(dispPlane);

// Create a basic sphere geometry
var sphereGeometry = new THREE.SphereGeometry(sphereRadius-0.75,32,32);
var dispSphere = new THREE.Mesh(sphereGeometry, planeMaterial);
scene.add(dispSphere);

var X=Y=10;
var rX=20
var rY=5;
loader.load( 'Component/unsullied.dae', function ( collada ) {
		
	soldat=collada.scene;

	for (var x=0;x<rX;x+=1)
	{
		for (var z=0;z<rY;z+=1)
		{
			var soldats = soldat.clone();
			var vectB=new THREE.Vector3(0,0,0);
			vectB.x=sphereRadius*Math.cos(2*Math.PI*x/rX);
			vectB.z=sphereRadius*Math.sin(2*Math.PI*x/rX);
			var vectD=new THREE.Vector3(0,0,0);
			vectD.y=sphereRadius*Math.sin(Math.PI*z/(rY*2));	
			var dist=sphereRadius*Math.cos(Math.PI*z/(rY*2))/vectB.length();
			var vectC=	vectB.multiplyScalar(dist);
			//soldats.position.x=vectB.x;
			//soldats.position.z=vectB.z;
			soldats.position.y=vectD.y;
			soldats.position.x=vectC.x;
			soldats.position.z=vectC.z;
			scene.add(soldats);
		}
	}

	for (var x=-X;x<X;x+=1)
	{
		for (var z=-Y;z<Y;z+=1)
		{
			var soldats = soldat.clone();
			soldats.position.set(x,0,z);
			scene.add(soldats);
		}
	}
	scene.add(soldats);


} );

	// Création de la variable pour le rendu
	renderer = new THREE.WebGLRenderer();
	renderer.setSize( largeur, hauteur ); // Taille de l'image de rendu

	renderer.setClearColor(new THREE.Color(0xFFFFFF),1.0);

	// Permet de tourner l'objet avec la souris
	controls = new THREE.OrbitControls( camera, renderer.domElement );

	camera.lookAt( scene.position );
	renderer.render( scene, camera );
}


// 2) Animation
//--------------
function animate() {
	// Lance l'animation
	requestAnimationFrame( animate );
	// Prend en compte l'interaction avec la souris
	controls.update();
	// Recalcule le rendu
	renderer.render( scene, camera )	
}


// Ajout du rendu à la page web
function addToDOM() {
	var container = document.getElementById("webGL");
	container.appendChild( renderer.domElement );
}


