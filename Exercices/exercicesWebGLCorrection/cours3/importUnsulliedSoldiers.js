var model;
importUnsulliedSoldier();



function importUnsulliedSoldier(){

    loaderSuccess = function(collada){
        console.log(collada)
        model = collada.scene

        model.traverse(function(child) {
            if (child.isMesh) {

                // // Setup our wireframe
                const wireframeGeometry = new THREE.WireframeGeometry(child.geometry);
                const wireframeMaterial = new THREE.LineBasicMaterial({color: 0x000000, opacity: 1});
                const wireframe = new THREE.LineSegments(wireframeGeometry, wireframeMaterial);

                wireframe.name = 'wireframe';	
                child.add(wireframe);
            }
        });
    }


    // Chargement d'un objet OBJ
    loader = new THREE.ColladaLoader()
    // Charger le fichier
    loader.load('Component/model.dae', loaderSuccess)

    return model;
}