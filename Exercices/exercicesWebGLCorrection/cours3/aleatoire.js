var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 0.1, 1000 );
camera.position.z = 25;

var renderer = new THREE.WebGLRenderer();
renderer.setSize( 500, 500 );


/* Controls */

controls = new THREE.OrbitControls(camera, renderer.domElement);
controls.enableDamping = true;
controls.dampingFactor = 0.25;
controls.enableZoom = false;

light = new THREE.DirectionalLight(0xffffff, 1.0);
light.position.set(-1,-1,0)
scene.add(light);

// var width=5;
// var height=4;
// var material = new THREE.MeshBasicMaterial( { color: 0xff0000 } );
// var cube = new THREE.CubeGeometry( width, height);
// var cubeMesh = new THREE.Mesh( cube, material );
// scene.add(cubeMesh);
ajout();




function animate() {

    requestAnimationFrame(animate);

    controls.update();

    render();

}

function render() {

    renderer.render(scene, camera);

}

function ajout(){
	
var numb=Math.floor(Math.random() * 5);

for (var cpt=0;cpt<numb;cpt++)
{
    var material = new THREE.MeshPhongMaterial();
    material.color.r=Math.random();
    material.color.g=Math.random();    
    material.color.b=Math.random();
    var shape=Math.floor(Math.random() * 2);
    if(shape==1)
    {
        var width=5*Math.random();
        var height=5*Math.random();
        var cube = new THREE.CubeGeometry( width, height);
        var mesh = new THREE.Mesh( cube, material );
    }
    else{
        var radius=5*Math.random();
        var sphere = new THREE.SphereGeometry(  radius, 32, 16);
        var mesh = new THREE.Mesh( sphere, material );
    }
    mesh.position.x=10-(20*Math.random());
    mesh.position.y=10-(20*Math.random());
    mesh.position.z=10-(20*Math.random());
    scene.add(mesh);
}

}

function addToDOM() {
    var container = document.getElementById("webGL");
    container.appendChild( renderer.domElement );
}

animate();