var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 0.1, 1000 );
camera.position.z = 25;

var renderer = new THREE.WebGLRenderer();
renderer.setSize( 1000, 1000 );

var height=[10,10,10,10,10]
var speed=[0,0,0,0,0]
/* Controls */

controls = new THREE.OrbitControls(camera, renderer.domElement);
controls.enableDamping = true;
controls.dampingFactor = 0.25;
controls.enableZoom = false;

light = new THREE.DirectionalLight(0xffffff, 1.0);
light.position.set(-1,-1,0)
scene.add(light);
renderer.setClearColor( 0xFFFFFF);
// var width=5;
// var height=4;
// var material = new THREE.MeshBasicMaterial( { color: 0xff0000 } );
// var cube = new THREE.CubeGeometry( width, height);
// var cubeMesh = new THREE.Mesh( cube, material );
// scene.add(cubeMesh);
var mesh= new Array(5);
for (var cpt=0;cpt<5;cpt++)
{
    var material = new THREE.MeshPhongMaterial();
    material.color.r=Math.random();
    material.color.g=Math.random();    
    material.color.b=Math.random();
    var radius=5*Math.random();
    var sphere = new THREE.SphereGeometry(  radius, 32, 16);
    mesh[cpt] = new THREE.Mesh( sphere, material );
    mesh[cpt].position.x=10-(20*Math.random());
    mesh[cpt].position.z=10-(20*Math.random());
    mesh[cpt].position.y=height[cpt];
    speed[cpt]=1+2*Math.random();
    scene.add(mesh[cpt]);
}





function animate() {
for (var cpt=0;cpt<5;cpt++)
{
    mesh[cpt].position.y=mesh[cpt].position.y-0.1*speed[cpt];
    mesh[cpt].scale.x=2*(20.-Math.abs(mesh[cpt].position.y))/20.;
    if (mesh[cpt].position.y<-20)
    {
        mesh[cpt].position.y=20;
    }
}
    requestAnimationFrame(animate);

    controls.update();

    render();

}

function render() {

    renderer.render(scene, camera);

}



function addToDOM() {
    var container = document.getElementById("webGL");
    container.appendChild( renderer.domElement );
}

animate();