"use strict"; // good practice - see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
////////////////////////////////////////////////////////////////////////////////
// Procedural texturing
// Your task is to change the fragment shader, and only that
// Edit the file named fragment.glsl in the tab above.
////////////////////////////////////////////////////////////////////////////////
/*global THREE, document, window, dat, $*/

var camera, scene, renderer, light;
var cameraControls, effectController, phongMaterial;
var clock = new THREE.Clock();
var start;
var material;

function fillScene() {
	scene = new THREE.Scene();
	scene.fog = new THREE.Fog( 0xAAAAAA, 2000, 4000 );

	// LIGHTS
	var ambientLight = new THREE.AmbientLight(0x333333); // 0.2

	light = new THREE.DirectionalLight(0xFFFFFF, 1.0);
	light.position.set(320, 390, 700);

	scene.add(ambientLight);
	scene.add(light);

	var teapotSize = 400;
	var loader= new THREE.TextureLoader();


	material = new THREE.ShaderMaterial( {
		uniforms: {
		    tExplosion: {
		      type: "t",
		       value: loader.load( 'explosion.png' )
		    },
		    time: { // float initialized to 0
		      type: "f",
		      value: 0.0
		    }
		},
	  	vertexShader: document.getElementById( 'vertexShader' ).textContent,
	  	fragmentShader: document.getElementById( 'fragmentShader' ).textContent
	} );
	material.side = THREE.DoubleSide;
	start = Date.now();

	var teapot = new THREE.Mesh(
		new THREE.TeapotGeometry(teapotSize, 20, true, true, true, true), material);

	var sphereGeo=new THREE.SphereGeometry(100,32,32);
	var sphere=new THREE.Mesh(sphereGeo,material);
	scene.add(sphere);
	//scene.add(teapot);
}


function init() {
	var canvasWidth = 846;
	var canvasHeight = 494;
	// For grading the window is fixed in size; here's general code:
	//var canvasWidth = window.innerWidth;
	//var canvasHeight = window.innerHeight;
	var canvasRatio = canvasWidth / canvasHeight;

	// RENDERER
	renderer = new THREE.WebGLRenderer( { antialias: true } );
	renderer.gammaInput = true;
	renderer.gammaOutput = true;
	renderer.setSize(canvasWidth, canvasHeight);
	renderer.setClearColor( 0xAAAAAA, 1.0 );

	// CAMERA
	camera = new THREE.PerspectiveCamera( 45, canvasRatio, 1, 80000 );
	camera.position.set(-600, 900, 1300);
	// CONTROLS
	cameraControls = new THREE.OrbitControls(camera, renderer.domElement);
	cameraControls.target.set(0, 0, 0);
}

function addToDOM() {
	var container = document.getElementById('webGL');
	var canvas = container.getElementsByTagName('canvas');
	if (canvas.length>0) {
		container.removeChild(canvas[0]);
	}
	container.appendChild( renderer.domElement );
}

function animate() {
	window.requestAnimationFrame(animate);
	render();
}

function render() {
	var delta = clock.getDelta();
	cameraControls.update(delta);
	material.uniforms[ 'time' ].value = .00025 * ( Date.now() - start );
	renderer.render(scene, camera);
}


// this is the main action sequence

try {
	init();
	fillScene();
	addToDOM();
	animate();
} catch(e) {
	var errorReport = "Your program encountered an unrecoverable error, can not draw on canvas. Error was:<br/><br/>";
}

