var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera( 30, window.innerWidth/window.innerHeight, 0.1, 1000 );
//camera.position.z = 40;


var start_time = Date.now();
// var mouseX = 0, mouseY = 0;

var renderer = new THREE.WebGLRenderer();
renderer.setSize( 800, 800 );

var mesh= new Array(5);
var speed= new Array(10);
/* Controls */

// controls = new THREE.OrbitControls(camera, renderer.domElement);
// controls.enableDamping = true;
// controls.dampingFactor = 0.25;
// controls.enableZoom = false;

var stats = new Stats();
stats.showPanel( 0 ); // 0: fps, 1: ms, 2: mb, 3+: custom
document.body.appendChild( stats.dom );

//scene.add( new THREE.AmbientLight( 0x555555 ) );

light = new THREE.DirectionalLight(0xffffff, 0.5);
light.position.set(1,1,0)
scene.add(light);

light2 = new THREE.DirectionalLight(0xffffff, 1.0);
light2.position.set(-1,1,0)
//scene.add(light2);

light2 = new THREE.DirectionalLight(0xffffff, 0.5);
light2.position.set(0,0,1)
scene.add(light2);


renderer.setClearColor( 0x4583B4, 1.0 );

// camera.position.z = 6000;

    geometry = new THREE.Geometry();

    var loader = new THREE.TextureLoader();
    var texture = loader.load( 'cloud10.png' ); 
    //var texture = THREE.ImageUtils.loadTexture( 'cloud10.png' );
    texture.magFilter = THREE.LinearMipMapLinearFilter;
    texture.minFilter = THREE.LinearMipMapLinearFilter;

    var fog = new THREE.Fog( 0x4584b4, - 100, 3000 );

    var material = new THREE.SpriteMaterial( { map: texture , color: 0xffffff } ); 

    for ( var i = 0; i < 8000; i++ ) {
        var sprite = new THREE.Sprite( material ); 
        // sprite.scale.set(10, 10, 1);
        // sprite.position.set(i*10,i*10,0);
        sprite.position.x = Math.random() * 1000 - 500;
        sprite.position.y = - Math.random() * 200 - 50;
        sprite.position.z = i;//-8000;
        sprite.rotation.z = Math.random() * Math.PI;
        sprite.scale.x = sprite.scale.y = 100*(Math.random() * 1.5 + 0.5);

        scene.add( sprite );
    }


// camera.position.x = 0;
//camera.position.y = 0;
camera.position.z = 500;

render();


            // function onDocumentMouseMove( event ) {

            //     mouseX = ( event.clientX - 800 ) * 0.25;
            //     mouseY = ( event.clientY - 800 ) * 0.15;

            // }


function animate() {

     requestAnimationFrame(animate);
     //controls.update();

camera.position.x = 0;
camera.position.y = 0;
camera.position.z = camera.position.z-0.5;
                    //var position = ( ( Date.now() - start_time ) * 0.03 ) % 8000;

                    // camera.position.x += ( mouseX - camera.position.x ) * 0.01;
                    // camera.position.y += ( - mouseY - camera.position.y ) * 0.01;
                    //camera.position.z = - position + 8000;
//camera.position.z = camera.position.z + 0.1;
// if (camera.position.z<-50)
// camera.position.z=0;
    stats.update();

    render();

}

function render() {
    renderer.render(scene, camera);

}

function ajout(){

}

function addToDOM() {
    var container = document.getElementById("webGL");
    container.appendChild( renderer.domElement );
}

animate();