/*************************************************************/
/* 		SCRIPT POUR AFFICHER UN FICHIER 3D AVEC TEXTURES     */
/*************************************************************/


//Largeur et hauteur de la fenêtre
var largeur=600;
var hauteur=600;
// Scène, caméra et variable pour le rendu
var scene, camera, renderer;

// 1) Initialisation de la scène
init();
// 2) Animation éventuelle
animate();


// 1) Initialisation de la scène
//------------------------------
function init() {
	// Création d'une scène vide
	scene = new THREE.Scene();
	// Création d'une caméra avec projection perspective
	camera = new THREE.PerspectiveCamera( 75, largeur/hauteur, 0.1, 1000 );


	//Création d'une lumière dirrectionelle de direction (0,0,1) donc suivant Z
	var directionalLight = new THREE.DirectionalLight( 0xffffff,5 ); //Création
	directionalLight.position.set( 0, 0, 1 ); // Ajout de la direction (Essayer suivant X et Y)
	scene.add( directionalLight ); // Ajout à la scène

	camera.position.z = 500; // Changement de position de la caméra (Essayer de diminuer et augmenter)


	var environment = new THREE.CubeTextureLoader()
	.setPath( 'loader2/ame_oasis/' )
	.load( ['oasisnight_lf.jpg', 'oasisnight_rt.jpg',
			'oasisnight_up.jpg', 'oasisnight_dn.jpg',
			'oasisnight_ft.jpg', 'oasisnight_bk.jpg' ]);

	scene.background = environment ;

	// for ( var i = -20; i < 20; i +=10 ) {
	// 	for ( var j = -20; j < 20; j +=10 ) {
	// 	  for ( var k = -20; k < 20; k +=10 ) {

	// 		var geometry = new THREE.SphereGeometry( 3, 32, 32 );
	// 		var material = new THREE.MeshPhongMaterial( {color: 0xffffff,envMap:environment,shininess: 100} );
	// 		var sphere = new THREE.Mesh( geometry, material );
	// 		sphere.position.set(i,j,k)
	// 		scene.add( sphere );
	// 		}
	// 	}
	// }
	var X=Y=5;
	var rX=7
	var rY=3;
	sphereRadius=10;
	for (var x=0;x<rX;x+=1)
	{
		for (var z=-rY;z<rY;z+=1)
		{
			var geometry = new THREE.SphereGeometry( 3, 32, 32 );
			var material = new THREE.MeshPhongMaterial( {color: 0xffffff,envMap:environment,shininess: 100} );
			var sphere = new THREE.Mesh( geometry, material );

			var vectB=new THREE.Vector3(0,0,0);
			vectB.x=sphereRadius*Math.cos(2*Math.PI*x/rX);
			vectB.z=sphereRadius*Math.sin(2*Math.PI*x/rX);
			var vectD=new THREE.Vector3(0,0,0);
			vectD.y=sphereRadius*Math.sin(Math.PI*z/(rY*2));	
			var dist=sphereRadius*Math.cos(Math.PI*z/(rY*2))/vectB.length();
			var vectC=	vectB.multiplyScalar(dist);
			//soldats.position.x=vectB.x;
			//soldats.position.z=vectB.z;
			sphere.position.y=vectD.y;
			sphere.position.x=vectC.x;
			sphere.position.z=vectC.z;
			scene.add( sphere );
		}
	}
	


	// a - Chargement d'une texture


	// Création de la variable pour le rendu
	renderer = new THREE.WebGLRenderer({ alpha: true });
	renderer.setSize( largeur, hauteur ); // Taille de l'image de rendu

	// Permet de tourner l'objet avec la souris
	controls = new THREE.OrbitControls( camera, renderer.domElement );
	camera.position.set( 25, 25, 25 );
	//camera.lookAt( scene.position );

	renderer.render( scene, camera );//renderer.setClearColorHex( 0xffffff, 1 );
}


// 2) Animation
//--------------
function animate() {

	// lance l'animation
	requestAnimationFrame( animate );
	// Prend en compte l'interaction avec la souris
	controls.update();
	// Recalcule le rendu
	renderer.render( scene, camera )	
}




// Ajout du rendu à la page web
function addToDOM() {
	var container = document.getElementById("webGL");
	container.appendChild( renderer.domElement );
}


