var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera( 30, window.innerWidth/window.innerHeight, 0.1, 1000 );
//camera.position.z = 40;


var start_time = Date.now();
// var mouseX = 0, mouseY = 0;

var renderer = new THREE.WebGLRenderer();
renderer.setSize( 800, 800 );

var mesh= new Array(5);
var speed= new Array(10);
/* Controls */

controls = new THREE.OrbitControls(camera, renderer.domElement);
controls.enableDamping = true;
controls.dampingFactor = 0.25;
controls.enableZoom = false;

var stats = new Stats();
stats.showPanel( 0 ); // 0: fps, 1: ms, 2: mb, 3+: custom
document.body.appendChild( stats.dom );

//scene.add( new THREE.AmbientLight( 0x555555 ) );

light = new THREE.DirectionalLight(0xffffff, 0.5);
light.position.set(1,1,0)
scene.add(light);

light2 = new THREE.DirectionalLight(0xffffff, 1.0);
light2.position.set(-1,1,0)
//scene.add(light2);

light2 = new THREE.DirectionalLight(0xffffff, 0.5);
light2.position.set(0,0,1)
scene.add(light2);


renderer.setClearColor( 0x4583B4, 1.0 );


    // geometry = new THREE.Geometry();

    // var loader = new THREE.TextureLoader();
    // var texture = loader.load( 'cloud10.png' ); 
    // //var texture = THREE.ImageUtils.loadTexture( 'cloud10.png' );
    // texture.magFilter = THREE.LinearMipMapLinearFilter;
    // texture.minFilter = THREE.LinearMipMapLinearFilter;

 

    // var material = new THREE.SpriteMaterial( { map: texture , color: 0xffffff } ); 

    // for ( var i = 0; i < 8000; i++ ) {
    //     var sprite = new THREE.Sprite( material ); 
    //     // sprite.scale.set(10, 10, 1);
    //     // sprite.position.set(i*10,i*10,0);
    //     sprite.position.x = Math.random() * 1000 - 500;
    //     sprite.position.y = - Math.random() * 200 - 50;
    //     sprite.position.z = i;//-8000;
    //     sprite.rotation.z = Math.random() * Math.PI;
    //     sprite.scale.x = sprite.scale.y = 100*(Math.random() * 1.5 + 0.5);

    //     scene.add( sprite );
    // }


                scene.fog = new THREE.FogExp2( 0x000000, 0.0008 );
                var materials = [];
                // var geometry = new THREE.Geometry();
                var textureLoader = new THREE.TextureLoader();
                var sprite1 = textureLoader.load( "textures/sprites/snowflake1.png" );
                var sprite2 = textureLoader.load( "textures/sprites/snowflake2.png" );
                var sprite3 = textureLoader.load( "textures/sprites/snowflake3.png" );
                var sprite4 = textureLoader.load( "textures/sprites/snowflake4.png" );
                var sprite5 = textureLoader.load( "textures/sprites/snowflake5.png" );
                // for ( i = 0; i < 10000; i ++ ) {
                //     var vertex = new THREE.Vector3();
                //     vertex.x = Math.random() * 2000 - 1000;
                //     vertex.y = Math.random() * 2000 - 1000;
                //     vertex.z = Math.random() * 2000 - 1000;
                //     geometry.vertices.push( vertex );
                // }           
                parameters = [
                    [ [1.0, 0.2, 0.5], sprite2, 20 ],
                    [ [0.95, 0.1, 0.5], sprite3, 15 ],
                    [ [0.90, 0.05, 0.5], sprite1, 10 ],
                    [ [0.85, 0, 0.5], sprite5, 8 ],
                    [ [0.80, 0, 0.5], sprite4, 5 ]
                ];
                for ( i = 0; i < parameters.length; i ++ ) {
                    color  = parameters[i][0];
                    sprite = parameters[i][1];
                    size   = parameters[i][2];
                    materials[i] = new THREE.SpriteMaterial( { map: sprite , color: 0xffffff } );
                    //materials[i] = new THREE.PointsMaterial( { size: size, map: sprite, blending: THREE.AdditiveBlending, depthTest: false, transparent : true } );
                    materials[i].color.setHSL( color[0], color[1], color[2] );
                    //particles = new THREE.Points( geometry, materials[i] );
                     
                      for ( j = 0; j < 10000; j ++ ) {
                        var particles = new THREE.Sprite( materials[i] ); 
                        particles.position.x = Math.random() * 2000 - 1000;
                        particles.position.y = Math.random() * 2000 - 1000;
                        particles.position.z = Math.random() * 2000 - 1000; 
                        particles.rotation.x = Math.random() * 6;
                        particles.rotation.y = Math.random() * 6;
                        particles.rotation.z = Math.random() * 6;
                        particles.scale.x = particles.scale.y =size ;
                        scene.add( particles );                      

                      }
                    // particles.rotation.x = Math.random() * 6;
                    // particles.rotation.y = Math.random() * 6;
                    // particles.rotation.z = Math.random() * 6;
                    // scene.add( particles );
                }

start=Date.now();
camera.position.z = 500;

render();



function animate() {

     requestAnimationFrame(animate);
     controls.update();


var time = (Date.now()-start) * 0.005;

                for ( i = 0; i < scene.children.length; i ++ ) {
                    var object = scene.children[ i ];
                    if ( object instanceof THREE.Sprite ) {  
                        var rotation = new THREE.Matrix4().makeRotationY(time);
                        object.applyMatrix(rotation);
                    }
                }
                for ( i = 0; i < materials.length; i ++ ) {
                    color = parameters[i][0];
                    h = ( 360 * ( color[0] + time ) % 360 ) / 360;
                    materials[i].color.setHSL( h, color[1], color[2] );
                }


    stats.update();

    render();

}

function render() {
    renderer.render(scene, camera);




}

function ajout(){

}

function addToDOM() {
    var container = document.getElementById("webGL");
    container.appendChild( renderer.domElement );
}

animate();