/*************************************************************/
/* 		SCRIPT POUR AFFICHER UN FICHIER 3D AVEC TEXTURES     */
/*************************************************************/


//Largeur et hauteur de la fenêtre
var largeur=600;
var hauteur=600;
// Scène, caméra et variable pour le rendu
var scene, camera, renderer;

// 1) Initialisation de la scène
init();
// 2) Animation éventuelle
animate();


// 1) Initialisation de la scène
//------------------------------
function init() {
	// Création d'une scène vide
	scene = new THREE.Scene();
	// Création d'une caméra avec projection perspective
	camera = new THREE.PerspectiveCamera( 75, largeur/hauteur, 0.1, 1000 );

	//Création d'une lumière ambiante
	var ambiantLight = new THREE.AmbientLight( 0xffffff ); //Création
	scene.add( ambiantLight ); // Ajout à la scène

	//Création d'une lumière dirrectionelle de direction (0,0,1) donc suivant Z
	var directionalLight = new THREE.DirectionalLight( 0xffffff,1 ); //Création
	directionalLight.position.set( 0, 0, 1 ); // Ajout de la direction (Essayer suivant X et Y)
	scene.add( directionalLight ); // Ajout à la scène

	camera.position.z = 300; // Changement de position de la caméra (Essayer de diminuer et augmenter)


	var environment = new THREE.CubeTextureLoader()
	.setPath( 'ame_oasis/' )
	.load( ['oasisnight_lf.jpg', 'oasisnight_rt.jpg',
			'oasisnight_up.jpg', 'oasisnight_dn.jpg',
			'oasisnight_ft.jpg', 'oasisnight_bk.jpg' ]);

	scene.background = environment ;



	var mtlLoader = new THREE.MTLLoader();
	mtlLoader.setPath('greek_vase2/');
	mtlLoader.load('greek_vase2.mtl', function(materials) {
	  materials.preload();
	  var objLoader = new THREE.OBJLoader();
	  //objLoader.setMaterials(materials);
	  objLoader.setPath('greek_vase2/');
	  objLoader.load('greek_vase2.obj', function(object) {
	  object.traverse( function ( child ) {
	    		if ( child instanceof THREE.Mesh ) {
	    			/* Début du processus pour avoir du lissage */
	    			 var geometry = new THREE.Geometry().fromBufferGeometry( child.geometry );
						geometry.computeFaceNormals();
						geometry.mergeVertices();
						geometry.computeVertexNormals();
						child.geometry = new THREE.BufferGeometry().fromGeometry( geometry );
					/* Fin du processus pour avoir du lissage */
					var textureLoader = new THREE.TextureLoader();
					// J'ai utilisé http://www.crazybump.com pour faire cette texture					
		            //child.material.normalMap= textureLoader.load( "greek_vase2/vt2_NRM.png" ); //Essayer avec et sans
					//child.material.normalScale= new THREE.Vector2( 0.5, 0.5 ); // Essayer de changer les valeurs
					child.material.envMap=environment;
					child.material.shininess= 10;
	    		}
	    });
	    scene.add(object);
	  });
	});

	// a - Chargement d'une texture


	// Création de la variable pour le rendu
	renderer = new THREE.WebGLRenderer({ alpha: true });
	renderer.setSize( largeur, hauteur ); // Taille de l'image de rendu

	// Permet de tourner l'objet avec la souris
	controls = new THREE.OrbitControls( camera, renderer.domElement );

	camera.lookAt( scene.position );

	renderer.render( scene, camera );//renderer.setClearColorHex( 0xffffff, 1 );
}


// 2) Animation
//--------------
function animate() {

	// lance l'animation
	requestAnimationFrame( animate );
	// Prend en compte l'interaction avec la souris
	controls.update();
	// Recalcule le rendu
	renderer.render( scene, camera )	
}




// Ajout du rendu à la page web
function addToDOM() {
	var container = document.getElementById("webGL");
	container.appendChild( renderer.domElement );
}


