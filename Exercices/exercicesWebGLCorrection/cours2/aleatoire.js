var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 0.1, 1000 );
camera.position.z = 25;

var renderer = new THREE.WebGLRenderer();
renderer.setSize( 500, 500 );
renderer.setClearColor( 0xFFFFFF);

/* Controls */

controls = new THREE.OrbitControls(camera, renderer.domElement);
controls.enableDamping = true;
controls.dampingFactor = 0.25;
controls.enableZoom = false;

light = new THREE.DirectionalLight(0xffffff, 10.0);
light.position.set(-1,-1,-1)
scene.add(light);

drawCube(0, 0, 0, 5, 5, 5, 0.5);

for (var cpt=0;cpt<10000;cpt++)
{
    var x=50*Math.random()-25;
    var y=50*Math.random()-25;
    var z=50*Math.random()-25;
    dist=x*x+y*y+z*z
    if (dist<1000)
        drawCube(x, y, z, x+2, y+2, z+2,1-dist/1000);
}
//ajout();


function drawCube(x1, y1, z1, x2, y2, z2, transparence) {

	var material, mesh;
	material = new THREE.MeshLambertMaterial( { vertexColors: THREE.VertexColors,transparent : true, opacity:transparence } );
	var cube = new THREE.Geometry();
    cube.vertices.push( new THREE.Vector3( x1, y1, z1 ) );
	cube.vertices.push( new THREE.Vector3( x2, y1,  z1 ) );
	cube.vertices.push( new THREE.Vector3( x2, y2,  z1 ) );
    cube.vertices.push( new THREE.Vector3( x1, y2,  z1 ) );
    cube.vertices.push( new THREE.Vector3( x1, y1, z2 ) );
	cube.vertices.push( new THREE.Vector3( x2, y1,  z2 ) );
	cube.vertices.push( new THREE.Vector3( x2, y2,  z2 ) );
    cube.vertices.push( new THREE.Vector3( x1, y2,  z2 ) );
    
    cube.faces.push( new THREE.Face3( 0, 2, 1 ) );
    cube.faces.push( new THREE.Face3( 0, 3, 2 ) );

    cube.faces.push( new THREE.Face3( 0, 1, 5 ) );
    cube.faces.push( new THREE.Face3( 0, 5, 4 ) );

    cube.faces.push( new THREE.Face3( 3, 0, 4 ) );
    cube.faces.push( new THREE.Face3( 3, 4, 7 ) );

    cube.faces.push( new THREE.Face3( 2, 3, 6 ) );
    cube.faces.push( new THREE.Face3( 3, 7, 6 ) );

    cube.faces.push( new THREE.Face3( 1, 2, 5 ) );
    cube.faces.push( new THREE.Face3( 5, 2, 6 ) );
    
    cube.faces.push( new THREE.Face3( 4, 5, 6 ) );
    cube.faces.push( new THREE.Face3( 4, 6, 7 ) );

    var color1 = new THREE.Color( 0xff0000 );
    cube.faces[0].vertexColors = [ color1, color1, color1 ];
    cube.faces[1].vertexColors = [ color1, color1, color1 ];

    color1 = new THREE.Color( 0x00ff00 );
    cube.faces[2].vertexColors = [ color1, color1, color1 ];
    cube.faces[3].vertexColors = [ color1, color1, color1 ];

    color1 = new THREE.Color( 0x0000ff );
    cube.faces[4].vertexColors = [ color1, color1, color1 ];
    cube.faces[5].vertexColors = [ color1, color1, color1 ];

    var color1 = new THREE.Color( 0xffff00 );
    cube.faces[6].vertexColors = [ color1, color1, color1 ];
    cube.faces[7].vertexColors = [ color1, color1, color1 ];

    color1 = new THREE.Color( 0x00ffff );
    cube.faces[8].vertexColors = [ color1, color1, color1 ];
    cube.faces[9].vertexColors = [ color1, color1, color1 ];

    color1 = new THREE.Color( 0xff00ff );
    cube.faces[10].vertexColors = [ color1, color1, color1 ];
    cube.faces[11].vertexColors = [ color1, color1, color1 ];

    cube.computeFaceNormals();
    cube.computeVertexNormals();

    mesh = new THREE.Mesh( cube, material );

	scene.add( mesh );
}

function animate() {

    requestAnimationFrame(animate);

    controls.update();

    render();

}

function render() {

    renderer.render(scene, camera);

}

function ajout(){
	
var numb=Math.floor(Math.random() * 5);

for (var cpt=0;cpt<numb;cpt++)
{
    var material = new THREE.MeshPhongMaterial();
    material.color.r=Math.random();
    material.color.g=Math.random();    
    material.color.b=Math.random();
    var shape=Math.floor(Math.random() * 2);
    if(shape==1)
    {
        var width=5*Math.random();
        var height=5*Math.random();
        var cube = new THREE.CubeGeometry( width, height);
        var mesh = new THREE.Mesh( cube, material );
    }
    else{
        var radius=5*Math.random();
        var sphere = new THREE.SphereGeometry(  radius, 32, 16);
        var mesh = new THREE.Mesh( sphere, material );
    }
    mesh.position.x=10-(20*Math.random());
    mesh.position.y=10-(20*Math.random());
    mesh.position.z=10-(20*Math.random());
    scene.add(mesh);
}

}

function addToDOM() {
    var container = document.getElementById("webGL");
    container.appendChild( renderer.domElement );
}

animate();