\babel@toc {french}{}
\contentsline {chapter}{\numberline {1}Fonctionnement des TD/TP}{5}{chapter.1}% 
\contentsline {chapter}{\numberline {2}Points, vecteurs et maillage}{7}{chapter.2}% 
\contentsline {chapter}{\numberline {3}Les couleurs et mat\IeC {\'e}riaux}{15}{chapter.3}% 
\contentsline {chapter}{\numberline {4}Les transformations}{19}{chapter.4}% 
\contentsline {chapter}{\numberline {5}Les matrices}{25}{chapter.5}% 
\contentsline {chapter}{\numberline {6}L'\IeC {\'e}clairage}{33}{chapter.6}% 
\contentsline {chapter}{\numberline {7}Le point de vue}{39}{chapter.7}% 
\contentsline {chapter}{\numberline {8}Les textures}{43}{chapter.8}% 
\contentsline {subsubsection}{Passage de nuage}{46}{Item.44}% 
\contentsline {subsubsection}{Temp\IeC {\^e}te de neige}{47}{Item.51}% 
\contentsline {chapter}{\numberline {9}Les shaders}{51}{chapter.9}% 
\contentsline {chapter}{\numberline {10}Les interactions}{59}{chapter.10}% 
\contentsline {chapter}{\numberline {11}Annexe}{63}{chapter.11}% 
