function createSupport2(){

  var bevelRadius = 1.9;
  
  var legMaterial = new THREE.MeshPhongMaterial( { shininess: 4 } );
  legMaterial.color.setHex( 0xAdA79b );
  legMaterial.specular.setRGB( 0.5, 0.5, 0.5 );

  var footMaterial = new THREE.MeshPhongMaterial( { color: 0x960f0b, shininess: 30 } );
  footMaterial.specular.setRGB( 0.5, 0.5, 0.5 );

  // base
  var cube = new THREE.Mesh(
    new THREE.BeveledBlockGeometry( 20+64+110, 4, 2*77+12, bevelRadius ), footMaterial );
  cube.position.x = -45;  // (20+32) - half of width (20+64+110)/2
  cube.position.y = 4/2;  // half of height
  cube.position.z = 0;  // centered at origin
  scene.add( cube );
// feet
  cube = new THREE.Mesh(
    new THREE.BeveledBlockGeometry( 20+64+110, 52, 6, bevelRadius ), footMaterial );
  cube.position.x = -45;  // (20+32) - half of width (20+64+110)/2
  cube.position.y = 52/2; // half of height
  cube.position.z = 77 + 6/2; // offset 77 + half of depth 6/2
  scene.add( cube );

  cube = new THREE.Mesh(
    new THREE.BeveledBlockGeometry( 20+64+110, 52, 6, bevelRadius ), footMaterial );
  cube.position.x = -45;  // (20+32) - half of width (20+64+110)/2
  cube.position.y = 52/2; // half of height
  cube.position.z = -(77 + 6/2);  // negative offset 77 + half of depth 6/2
  scene.add( cube );

  cube = new THREE.Mesh(
    new THREE.BeveledBlockGeometry( 64, 104, 6, bevelRadius ), footMaterial );
  cube.position.x = 0;  // centered on origin along X
  cube.position.y = 104/2;
  cube.position.z = 77 + 6/2; // negative offset 77 + half of depth 6/2
  scene.add( cube );

  cube = new THREE.Mesh(
    new THREE.BeveledBlockGeometry( 64, 104, 6, bevelRadius ), footMaterial );
  cube.position.x = 0;  // centered on origin along X
  cube.position.y = 104/2;
  cube.position.z = -(77 + 6/2);  // negative offset 77 + half of depth 6/2
  scene.add( cube );

  // legs
  cube = new THREE.Mesh(
    new THREE.BeveledBlockGeometry( 60, 282+4, 4, bevelRadius ), legMaterial );
  cube.position.x = 0;  // centered on origin along X
  cube.position.y = 104 + 282/2 - 2;
  cube.position.z = 77 + 6/2; // negative offset 77 + half of depth 6/2
  scene.add( cube );

  cube = new THREE.Mesh(
    new THREE.BeveledBlockGeometry( 60, 282+4, 4, bevelRadius ), legMaterial );
  cube.position.x = 0;  // centered on origin along X
  cube.position.y = 104 + 282/2 - 2;
  cube.position.z = -(77 + 6/2);  // negative offset 77 + half of depth 6/2
  scene.add( cube );
}